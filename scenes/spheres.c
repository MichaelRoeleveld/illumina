#pragma once

#include "../src/vector.h"

// poisson disk (sphere?) sampling
static inline
int isValid(vec3 candidate, vec3 region, float cellSize, float radius, vec3 *points, int gridLengthX, int gridLengthY, int gridLengthZ, int grid[gridLengthX][gridLengthY][gridLengthZ] ){
	if (candidate.x < 0 || candidate.x > region.x ||
		candidate.y < 0 || candidate.y > region.y ||
		candidate.z < 0 || candidate.z > region.z){
		return 0;
	}

	int cellX = (int)(candidate.x/cellSize);
	int cellY = (int)(candidate.y/cellSize);
	int cellZ = (int)(candidate.z/cellSize);
	#define max(a,b) (a>b?a:b)
	#define min(a,b) (a<b?a:b)
	for(int x = max(0,cellX-2); x<min(cellX+2,gridLengthX); x++){
		for(int y = max(0,cellY-2); y<min(cellY+2,gridLengthY); y++){
			for(int z = max(0,cellZ-2); z<min(cellZ+2,gridLengthZ); z++){
				int idx = grid[x][y][z]-1;
				if(idx==-1){continue;}
				vec3 dir = sub3(candidate, points[idx]);
				if(dot3(dir,dir) < radius*radius){
					return 0;
				}
			}
		}
	}
	return 1;
}

int sample(vec3 *out, int outSize, int radius, vec3 region){
	float cellSize = radius/sqrtf(2.f);
	int xres = ceilf(region.x/cellSize);
	int yres = ceilf(region.y/cellSize);
	int zres = ceilf(region.z/cellSize);
	int grid[xres][yres][zres];
	memset(grid, 0, sizeof(grid));

	int spawnSites[outSize]; // indices into "out" of points which are spawn sites
	memset(spawnSites, 0, sizeof(spawnSites));
	int numSpawns = 1;
	int numSamples = 1;
	out[0] = scale3(region,random01());

	#ifdef DEBUG
	printf("initial point ");printvec(out[0]);
	#endif

	int rejectThreshold = 50;
	while(numSpawns && numSamples < outSize){
		int idx = random()%numSpawns;

		#define randrange(x,y)(random01()*(y-x)+x)

		for(int i=0;i<rejectThreshold; i++){
			float r = radius*sqrtf(randrange(1,4));
			vec3 dir = random3OnUnitSphere();
			vec3 candidate = add3(out[spawnSites[idx]], scale3(dir, r));

			if(isValid(candidate, region, cellSize, radius, out, xres, yres, zres, grid)){
				spawnSites[numSpawns++] = numSamples;
				out[numSamples++] = candidate;
				grid[(int)(candidate.x/cellSize)][(int)(candidate.y/cellSize)][(int)(candidate.z/cellSize)] = numSamples;
				goto accept;
			}
		}
		// remove this spawnpoint by Swap Last and Truncate array
		spawnSites[idx] = spawnSites[--numSpawns];

		accept:;
		#ifdef DEBUG
		printf("point %d: ",numSamples);printvec(out[numSamples-1]);
		#endif
	}
	#ifdef DEBUG
	printf("[sampler] sampled %d points at %p\n", numSamples, (void*)out);
	#endif
	return numSamples;
}

void setupDemoScene(void){

	printf("Setting up scene...\n");

	int max = 999;
	float vel = 50.f;
	float size = 10.f;
	float deviation = 0;//0.5f; // randomsize = size * (1 +- deviation)
	camera *cam = workspace->cam;
	workspace->bkgTexture = blackBkg;

	float scale = 32;
	vec3 region = scale3((vec3){16,9,9}, scale);

	vec3 lookat = scale3(region, 0.5);
	vec3 origin = add3(scale3(region, 0.5),(vec3){0,0,11*scale});

	initCamera(cam, origin, lookat, cam->aspect, cam->fov);

	material materials[9];
	materials[0]=(material){.color = scale3(rgb2vec("ffffff"),15)}; // light
	materials[1]=(material){.color = rgb2vec("eeeeee"), .reflectance=1.f}; // mirror beads
	materials[2]=(material){.color = rgb2vec("a9e8dd"), .reflectance=0.04f, .IOR=1.3f}; // glass/mineral beads
	materials[3]=(material){.color = rgb2vec("2a75b5"), .reflectance=1.f, .roughness=0.75f}; // blue
	materials[4]=(material){.color = rgb2vec("ff7f34"), .reflectance=1.f, .roughness=1.f}; // orange
	materials[5]=(material){.color = rgb2vec("313131"), .reflectance=1.f, .roughness=1.f}; // matte
	materials[6]=(material){.color = rgb2vec("474747"), .reflectance=1.f, .roughness=1.f}; // matte
	materials[7]=(material){.color = rgb2vec("636363"), .reflectance=1.f, .roughness=1.f}; // matte
	materials[8]=(material){.color = rgb2vec("898989"), .reflectance=1.f, .roughness=1.f}; // matte

	// sample up to 'max' random points to spawn spheres at, while guaranteeing at least size*n free space around that point
	vec3 centers[max];
	int n = sample(centers, max, size*5.f, region);

	// generate spheres
	for(int i=0; i<n; i++){
		material *sphereMaterial = &materials[rand()%9];
		#ifdef USE_SAMPLING_ORDER_COLOR_GRADIENT
		sphereMaterial = calloc(1, sizeof *sphereMaterial);
		sphereMaterial->color = (vec3){(float)i/n, 0, n-(float)i/n};
		#endif
		makeSphere(
			workspace,
			centers[i],
			randomf(size*(1.f+deviation), size*(1.f-deviation)),
			scale3(random3OnUnitSphere(),vel),
			sphereMaterial
		);
	}

	printf("Set up scene succesfully\n");
}
