#pragma once

#include <math.h>
#include <ieee754.h>

#include "random.h"

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif
#define mysqrt sqrtf

#define NEAR_ZERO 0.00000005
// for dealing with comparisons with floating point errors, also known as kEpsilon (pretentious name)

float sqrt_est(float x) {
	union ieee754_float b = *(union ieee754_float*)&x;

	int exponent = b.ieee.exponent;
	exponent -= IEEE754_FLOAT_BIAS;
	exponent /= 2;
	exponent += IEEE754_FLOAT_BIAS;
	b.ieee.exponent  = (unsigned) exponent;

	float guess = *(float*)&b;

	return guess - (guess * guess - x) / (2 * guess); 
}

static inline float lerpf(float a, float b, float t){
	return (1.f-t)*a + t*b;
}

static inline float randomf(float min, float max){
	return min + (max - min)*random01();
}

static inline float clampf(float a, float min, float max){
	// force a to be inside of inclusive range [min, max]
	const float t = a < min ? min : a;
	return t > max ? max : t;
}

static inline int solveQuadratic(float a, float b, float c, float *restrict x0, float *restrict x1)
{
	float discr = b*b - 4.f*a*c;
	if (discr < 0.f){
		return 0;
	}
	else
	{
		if (fabsf(discr) < NEAR_ZERO){ // discr == 0
			*x0 = *x1 = - 0.5f * b / a;
		} else {
			float q;
			if (b > 0.f){
				q = -0.5f * (b + mysqrt(discr));
			} else {
				q = -0.5f * (b - mysqrt(discr));
			}
			
			*x0 = q / a;
			*x1 = c / q;
		}
	}

	return 1;
}
