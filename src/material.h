typedef struct {
	vec3 (*texture)(vec3); // points to a texture function, sampled at world space hitpoint, overrides color if not NULL
	vec3 color;            // RGB 0 to 1. Larger than 1 -> emissive material

	float gloss_reflectance; // 0 for no gloss
	float gloss_roughness;   // 0.05 to 0.01 for satin finish, 0 for mirror finish

	float reflectance; // ratio of incoming light that is reflected away (1-r is transmitted)
	float roughness;   // reflection and refraction scattering for brushed metal-like surfaces; smudgy reflections
	float IOR;         // index of refraction
} material;
