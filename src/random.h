#pragma once

#include <stdint.h>
#include <threads.h>

#define SEED_64 0xb5ad4eceda1ce2a9
#define SEED_64_2 0x0EFE383262B6D10B
#define SEED_32 0xb5ad4ece

#define RANDOM_MAX 0xffffffff

#define random() xoroshiro128plus()//msws()//rand()
#define random01() ((float)random()/(float)RANDOM_MAX)

uint32_t xorshift32(void){
	thread_local static uint32_t x = SEED_32;
	x ^= x << 13;
	x ^= x >> 17;
	x ^= x << 5;
	return x;
}

uint64_t xorshift64(void){
	thread_local static uint64_t x = SEED_64;
	x ^= x << 13;
	x ^= x >> 7;
	x ^= x << 17;
	return x;
}

uint32_t msws(void){
	thread_local static uint64_t x = 0, w = 0, s = SEED_64;
	x *= x; 
	x += (w += s); 
	x = (x>>32) | (x<<32);
	return x;
}

uint32_t xoroshiro128plus(void)
{
	thread_local static uint64_t s0 = SEED_64;
	thread_local static uint64_t s1 = SEED_64_2;
	uint64_t result = s0 + s1;
	s1 ^= s0;
	s0 = ((s0 << 55) | (s0 >> 9)) ^ s1 ^ (s1 << 14);
	s1 = (s1 << 36) | (s1 >> 28);
	return (uint32_t)(result);
}

// rdrand is really slow...

inline uint32_t rdrand32(void){
	uint32_t x;
	asm("rdrand %0;":"=r"(x));
	return x;
}

inline uint64_t rdrand64(void){
	uint64_t x;
	asm("rdrand %0;":"=r"(x));
	return x;
}
