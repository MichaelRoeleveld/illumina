
struct __attribute__((packed)) BITMAPFILEHEADER {
	uint16_t bfType; // = 19778 "BM"
	uint32_t bfSize; // specifies the size of the file in bytes.
	uint32_t bfReserved;
	uint32_t bfOffBits; // specifies the offset from the beginning of the file to the bitmap data.
};

struct __attribute__((packed)) BITMAPINFOHEADER {
	uint32_t biSize; // specifies the size of the BITMAPINFOHEADER structure, in bytes.
	uint32_t biWidth; // specifies the width of the image, in pixels.
	uint32_t biHeight; // specifies the height of the image, in pixels.
	uint16_t biPlanes; // specifies the number of planes of the target device, must be set to 1.
	uint16_t biBitCount; // specifies the number of bits per pixel.
	uint32_t biCompression; // Specifies the type of compression, usually set to zero (no compression).
	uint32_t biSizeImage; // specifies the size of the image data, in bytes. If there is no compression, it is valid to set this member to zero.
	uint32_t biXPelsPerMeter; // specifies the the horizontal pixels per meter on the designated targer device, usually set to zero.
	uint32_t biYPelsPerMeter; // specifies the the vertical pixels per meter on the designated targer device, usually set to zero.
	uint32_t biClrUsed; // specifies the number of colors used in the bitmap, if set to zero the number of colors is calculated using the biBitCount member.
	uint32_t biClrImportant; // specifies the number of color that are 'important' for the bitmap, if set to zero, all colors are important.
}; 
/*
uint8_t* SDLTextureToBMP(int xres, int yres, SDL_Texture* texture, int pitch){

	uint32_t rowsize = (3*xres + 3)/4 * 4; // align to 4 bytes
	uint32_t pixarrsize = rowsize*yres;
	uint8_t* buffer;
	uint8_t* pixarr = buffer + sizeof(struct BITMAPINFOHEADER) + sizeof(struct BITMAPFILEHEADER);

	struct BITMAPFILEHEADER bfh = {
		.bfType = 19778,
		.bfSize = sizeof(struct BITMAPFILEHEADER) + sizeof(struct BITMAPINFOHEADER) + pixarrsize,
		.bfReserved = 0,
		.bfOffBits = sizeof(struct BITMAPFILEHEADER) + sizeof(struct BITMAPINFOHEADER)
	};
	struct BITMAPINFOHEADER bih = {
		.biSize = sizeof(struct BITMAPINFOHEADER),
		.biWidth = xres,
		.biHeight = yres,
		.biPlanes = 1,
		.biBitCount = 24,
		.biCompression = 0,
		.biSizeImage = pixarrsize,
		.biXPelsPerMeter = 69420,
		.biYPelsPerMeter = 69420,
		.biClrUsed = 0,
		.biClrImportant = 0
	};

	return buffer;
}*/

void bitmap(int xres, int yres, SDL_Texture** texture, int pitch){

	puts("Attempting to make screenshot...");
	unsigned int rowsize = (3*xres + 3)/4 * 4; // align to 4 bytes
	unsigned int pixarrsize = rowsize*yres;
	unsigned char* pixarr = calloc(yres, rowsize);

	printf("  res: %dx%d\n  texture: %p\n  *texture: %p\n  pitch: %d\n  rowsize: %d\n  pixarrsize: %d\n  pixarr: %p\n",
		xres,yres,
		(void*)texture,
		(void*)*texture,
		pitch,
		rowsize,
		pixarrsize,
		(void*)pixarr);

	puts("Filling headers");
	struct BITMAPFILEHEADER bfh = {
		.bfType = 19778,
		.bfSize = sizeof(struct BITMAPFILEHEADER) + sizeof(struct BITMAPINFOHEADER) + pixarrsize,
		.bfReserved = 0,
		.bfOffBits = sizeof(struct BITMAPFILEHEADER) + sizeof(struct BITMAPINFOHEADER)
	};
	struct BITMAPINFOHEADER bih = {
		.biSize = sizeof(struct BITMAPINFOHEADER),
		.biWidth = xres,
		.biHeight = yres,
		.biPlanes = 1,
		.biBitCount = 24,
		.biCompression = 0,
		.biSizeImage = pixarrsize,
		.biXPelsPerMeter = 69420,
		.biYPelsPerMeter = 69420,
		.biClrUsed = 0,
		.biClrImportant = 0
	};

	puts("Copying pixelarray");

	uint8_t *pixels;
	int test = SDL_LockTexture(*texture, NULL, (void **) &pixels, &pitch);
	if (test != 0){
		puts("SDL_LockTexture != 0 (failed) - could not capture screenshot!");
		puts(SDL_GetError());
		return;
	}

	for (int y = 0; y < yres; ++y)
	{
		for (int x = 0; x < xres; ++x)
		{
			int idst = rowsize*y + x*3;
			int isrc = pitch*(yres-y-1) + x*3; // flip the image vertically
			pixarr[idst  ] = pixels[isrc+2]; // bgr to rgb
			pixarr[idst+1] = pixels[isrc+1];
			pixarr[idst+2] = pixels[isrc+0];
		}
	}

	SDL_UnlockTexture(*texture);	

	puts("Preparing filename");
	pid_t pid = getpid();
	int microtime = clock()/(CLOCKS_PER_SEC/1000);
	char namebuf[5+1+8+4+1];	
	sprintf(namebuf, "%05d_%08d.bmp", pid, microtime);

	printf("Outputting to file \"%s\"\n", namebuf);
	// write the data in binary mode to stdout
	FILE *const out = fopen(namebuf, "wb");
	fwrite(&bfh, sizeof bfh, 1, out); // write 1 bfh
	fwrite(&bih, sizeof bih, 1, out); // write 1 bih
	fwrite(pixarr, rowsize, yres, out); // write YRES rows
	fclose(out);
	free(pixarr);
}
