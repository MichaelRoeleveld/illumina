#pragma once

// pseudo inheritance: each primitive has the pointer to material at 0 bytes offset so that mat** and primitive* are interchangeable
typedef struct {
	material *mat;
} primitive;

typedef struct {
	material *mat;
	vec3 v0;
	vec3 v1;
	vec3 v2;
	vec3 normal;
} triangle;

static inline vec3 nTriangle(triangle* t){ // surface normal
	vec3 edge0 = sub3(t->v1, t->v0);
	vec3 edge1 = sub3(t->v2, t->v0);

	return normalize(cross3(edge0, edge1));
}

typedef struct {
	material *mat;
	vec3 position;
	vec3 velocity;
	float radius;
	float mass;
} sphere;

static inline vec3 nSphere(sphere * sph, vec3 p){ // surface normal
	return normalize(sub3(p, sph->position));
}
