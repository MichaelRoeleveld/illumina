struct thread_data{ 
	// communication line between ui thread and main thread
	int * demoRunning;
	SDL_Window * window;
	scene * workspace;
	SDL_Texture ** texturePtr;
	int pitch, xres, yres;
};

