#pragma once

#include "vector.h"

// written by Michael Roeleveld as part of the Illumina engine
// 02 feb 2020

typedef struct {
	vec3 min, max;
} AABB;

// make an AABB around an array of triangles
AABB AABBFromTriangles(triangle *triangles, size_t n) {
	vec3 min, max;
	min = max = triangles[0].v0;

	for (size_t i = 0; i < n; ++i) {
		const vec3 vertices[3] = {triangles[i].v0, triangles[i].v1, triangles[i].v2};
		for (int j = 0; j < 3; ++j) {
			min.x = fmin(min.x, vertices[j].x);
			max.x = fmax(max.x, vertices[j].x);
			min.y = fmin(min.y, vertices[j].y);
			max.y = fmax(max.y, vertices[j].y);
			min.z = fmin(min.z, vertices[j].z);
			max.z = fmax(max.z, vertices[j].z);
		}
	}
	const float e = NEAR_ZERO;
	min = sub3(min, (vec3){e, e, e});
	max = add3(max, (vec3){e, e, e});

	return (AABB){min,max};
}

AABB mergeAABB(AABB a, AABB b){
	vec3 min = (vec3){
		fminf(a.min.x, b.min.x),
		fminf(a.min.y, b.min.y),
		fminf(a.min.z, b.min.z)
	};
	vec3 max = (vec3){
		fmaxf(a.max.x, b.max.x),
		fmaxf(a.max.y, b.max.y),
		fmaxf(a.max.z, b.max.z)
	};

	return (AABB){min,max};
}

static inline
AABB AABBFromSphere(sphere* s) {
	AABB box;
	box.min = (vec3){s->position.x - s->radius, s->position.y - s->radius, s->position.z - s->radius};
	box.max = (vec3){s->position.x + s->radius, s->position.y + s->radius, s->position.z + s->radius};
	return box;
}

AABB AABBFromSpheres(sphere* spheres, size_t sphereCount) {
	AABB box;
	box.min = (vec3){FLT_MAX, FLT_MAX, FLT_MAX};
	box.max = (vec3){-FLT_MAX, -FLT_MAX, -FLT_MAX};

	for (size_t i = 0; i < sphereCount; i++) {
		AABB sphereBox = AABBFromSphere(&spheres[i]);
		box = mergeAABB(sphereBox,box);
	}

	return box;
}

void printAABB(AABB* a){
	printf("AABB:\n\tmin = ");
	printvec(a->min);
	printf("\tmax = ");
	printvec(a->max);
}
