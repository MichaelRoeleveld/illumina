CC=gcc
SDL2FLAGS=$(shell pkg-config sdl2 --cflags --libs)
CFLAGS=-std=gnu18 -fopenmp -Wall -Wextra -pedantic -Wshadow -Wstrict-aliasing -Wstrict-overflow -march=native -Wno-coverage-mismatch 
LFLAGS=-lm

.PHONY: all clean debug profile fast

all: main

main: main.c
	${CC} ${CFLAGS} -Ofast -o bin/illumina.exe $< ${SDL2FLAGS} ${LFLAGS}

fast: main.c
	${CC} ${CFLAGS} -ffast-math -fno-math-errno -fno-rounding-math -funsafe-math-optimizations -ffinite-math-only -funroll-loops -mrecip=all -flto -fprofile-use -Ofast -o bin/illumina.exe $< ${SDL2FLAGS} ${LFLAGS}
	./bin/illumina.exe 256 256 4

debug: main.c
	${CC} ${CFLAGS} -Og -ggdb -DDEBUG -fno-inline -fno-omit-frame-pointer -fsanitize=address,undefined -o bin/illumina.exe $< ${SDL2FLAGS} ${LFLAGS}
	./bin/illumina.exe 256 256 4

profile: main.c
	${CC} ${CFLAGS} -Og -g -pg -fprofile-generate -fno-inline -fno-omit-frame-pointer -o bin/illumina.exe $< ${SDL2FLAGS} ${LFLAGS}
	./bin/illumina.exe 256 256 4

run: bin/illumina.exe
	./bin/illumina.exe 640 360 3

clean:
	rm -f bin/* 
